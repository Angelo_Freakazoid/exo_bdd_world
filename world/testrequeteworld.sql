-- Liste des type de gouvernement avec nombre de pays pour chaque.

SELECT GovernmentForm, COUNT(*) AS Nombre_de_pays
FROM country
GROUP BY GovernmentForm ORDER BY Nombre_de_pays DESC;

-- D’apres la BDD, combien de personne dans le monde parle anglais ?

SELECT SUM(c.Population * cl.Percentage / 100) AS Total_personne_parlant_Anglais
FROM country c
JOIN countrylanguage cl ON c.Code = cl.CountryCode
WHERE cl.Language = 'English';

-- Faire la liste des langues avec le nombre de locuteur, de la plus parlée à la moins parlée.

SELECT cl.Language, SUM(c.Population * cl.Percentage / 100) AS Total_Locuteur
FROM countrylanguage cl
JOIN country c ON cl.CountryCode = c.Code
GROUP BY cl.Language
ORDER BY Total_Locuteur DESC;

-- Trouver la superfice de la Suisse.

SELECT SurfaceArea FROM country WHERE Name = 'Switzerland';

-- Faire la liste des pays qui ont plus 10 000 000 d’hab. avec leur capitale et le % de la population qui habite dans la capitale.

SELECT 
    co.Name AS Nom_pays, 
    ci.Name AS Nom_capitale, 
    co.Population AS Population_pays, 
    ci.Population AS Population_capitale, 
    (ci.Population / co.Population * 100) AS Pourcentage_pop_dans_capitale
FROM country co JOIN city ci ON co.Capital = ci.ID WHERE co.Population > 10000000;

-- Liste des 10 pays avec le plus fort taux de croissance entre n et n-1 avec le % de croissance

SELECT Name, GNP, GNPOld, ((GNP - GNPOld) / GNPOld * 100) AS Taux_de_croissance
FROM country WHERE GNPOld > 0 
ORDER BY Taux_de_croissance DESC LIMIT 10;

-- Liste des pays plurilingues avec pour chacun le nombre de langues parlées.

SELECT 
    c.Name AS Nom_pays, 
    COUNT(cl.Language) AS Nombre_de_langues
FROM country c
JOIN countrylanguage cl ON c.Code = cl.CountryCode
GROUP BY c.Name HAVING COUNT(cl.Language) > 1
ORDER BY Nombre_de_langues DESC;

-- Liste des pays avec plusieurs langues officielles, le nombre de langues officielle et le nombre de langues du pays.

SELECT 
    c.Name AS Nom_pays, 
    COUNT(cl.Language) AS Langues_totales,
    SUM(CASE WHEN cl.IsOfficial = 'T' THEN 1 ELSE 0 END) AS Langues_officieles
FROM country c
JOIN countrylanguage cl ON c.Code = cl.CountryCode
GROUP BY c.Name HAVING SUM(CASE WHEN cl.IsOfficial = 'T' THEN 1 ELSE 0 END) > 1;

-- Liste des langues parlées en France avec le % pour chacune.

SELECT Language, Percentage
FROM countrylanguage
WHERE CountryCode = (SELECT Code FROM country WHERE Name = 'France');

-- Liste des langues parlées en Chine avec le % pour chacune.
 
SELECT Language, Percentage
FROM countrylanguage
WHERE CountryCode = (SELECT Code FROM country WHERE Name = 'China');

-- Liste des langues parlées en Etats-Unis avec le % pour chacune.

SELECT Language, Percentage
FROM countrylanguage
WHERE CountryCode = (SELECT Code FROM country WHERE Name = 'United States');

-- Liste des langues parlées en Royaume-Uni avec le % pour chacune.

SELECT Language, Percentage
FROM countrylanguage
WHERE CountryCode = (SELECT Code FROM country WHERE Name = 'United Kingdom');

-- Pour chaque région quelle est la langue la plus parler et quel pourcentage de la population la parle.


SELECT 
    sub.Region,
    sub.Language,
    sub.Pourcentage
FROM (
    SELECT 
        lr.Region,
        lr.Language,
        (lr.TotalPopulationSpeakingLanguage * 100) / pr.TotalPopulation AS Pourcentage,
        ROW_NUMBER() OVER (PARTITION BY lr.Region ORDER BY (lr.TotalPopulationSpeakingLanguage * 100) / pr.TotalPopulation DESC) AS rn
    FROM (
        SELECT 
            c.Region,
            cl.Language,
            SUM((cl.Percentage / 100) * c.Population) AS TotalPopulationSpeakingLanguage
        FROM 
            countrylanguage cl
        JOIN 
            country c ON cl.CountryCode = c.Code
        GROUP BY 
            c.Region, cl.Language
    ) AS lr
    JOIN (
        SELECT 
            c.Region,
            SUM(c.Population) AS TotalPopulation
        FROM 
            country c
        GROUP BY 
            c.Region
    ) AS pr ON lr.Region = pr.Region
) AS sub
WHERE 
    sub.rn = 1
ORDER BY 
    sub.Region;

-- Est-ce que la somme des pourcentages de langues parlées dans un pays est égale à 100 ? Pourquoi ?

SELECT 
    c.Name AS CountryName, 
    SUM(cl.Percentage) AS Total_pourcentage_langue
FROM country c
JOIN countrylanguage cl ON c.Code = cl.CountryCode
WHERE c.Name = 'France'
GROUP BY c.Name;

