# Analyse de la BDD world

## Varchar ou char :

![Différence entre varchar et char dans une table.](varchar_or_char.png "Explication varchar / char")

## Le MLD de la BDD :

![Voici le diagrame de la BDD.](city_country_mld.png "Diagrame")

## Explication des tables :

### la bdd 'world' contient 3 tables :

penser à lancer le fichier acces.sql pour donner les droits à toutes les bdd.

#### Table 1 : city, 
    cette table contient 5 colonnes :
        - colonne 1 : ID (int NOT NULL AUTO_INCREMENT): ID de type intier (INT) ne peut pas contenir de valeurs nulles (NOT NULL) MySQL générera automatiquement une valeur unique pour chaque nouvelle entrée (AUTO_INCREMENT) cette colonne est désignée comme clé primaire (PRIMARY KEY).
        - colonne 2 : Name (char(35) NOT NULL DEFAULT ''): Name nom de type caractère (char) avec une longueur fixe de 35 caractères. NOT NULL et DEFAULT '' indentique à la colonne précédente. Name indique le nom des villes, exemple : 'Kabul' première entrée de la table.
        - colonne 3 : CountryCode (char(3) NOT NULL DEFAULT ''): ContryCode code ISO alpha 3 servant à identifier un pays (ce code contient 3 caractères d'ou le char(3)), exemple : AFG pour la première entrèe qui correspond au pays Afghanistan.
        - colonne 4 : District (char(20) NOT NULL DEFAULT ''): District indique les différentes provinces (des fois des districts, des fois des municipalités, des fois des départements) d'un pays.
        - colonne 5 : Population (int NOT NULL DEFAULT '0'): cette colonne stocke la population en tant qu'entier (INT), Elle ne peut pas être nulle et par défaut, elle est définie à 0, ce qui est logique pour une donnée numérique comme la population.



#### Table 2 : country,
    cette table contient 15 colonnes :
    - colonne 1 : `Code (char(3) NOT NULL DEFAULT '')`: code ISO alpha 3 servant à identifier un pays (ce code contient 3 caractères d'ou le char(3)), exemple : ABW pour la première entrèe qui correspond au pays Aruba.
    - coonne 2 : `Name (char(52) NOT NULL DEFAULT '')`: Name est une colonne de type char de longueur 52 caractères, contenant le nom du pays.
    - colonne 3 : `Continent (enum(...) NOT NULL DEFAULT 'Asia')`: Continent est un type énuméré (enum), limitant les valeurs à une liste spécifiée de continents. NOT NULL et DEFAULT 'Asia' indiquent qu'une valeur doit être présente et que 'Asia' est la valeur par défaut.
    - colonne 4 : `Region (char(26) NOT NULL DEFAULT '')`: Région du monde,
    - colonne 5 : `SurfaceArea (decimal(10,2) NOT NULL DEFAULT '0.00')`: SurfaceArea stocke la superficie, utilisant le type decimal avec 10 chiffres au total et 2 chiffres après la virgule. La colonne ne peut pas être nulle et a une valeur par défaut de 0.00.
    - colonne 6 : `IndepYear (smallint DEFAULT NULL)`: IndepYear stocke l'année d'indépendance en tant que nombre entier court (smallint). Cette colonne peut contenir des valeurs nulles (DEFAULT NULL), ce qui est utile pour les pays sans année d'indépendance connue ou applicable.
    - colonne 7 : `Population (int NOT NULL DEFAULT '0')`: Population est un entier (int), indiquant la population du pays. Il est non nul et a une valeur par défaut de 0.
    - colonne 8-9-10 : `LifeExpectancy, GNP, GNPOld (decimal(3,1), decimal(10,2), decimal(10,2) DEFAULT NULL)`: Ces colonnes stockent respectivement l'espérance de vie, le PNB (Produit National Brut), et l'ancien PNB. Elles utilisent le type decimal et peuvent être nulles, ce qui est pratique pour les pays où certaines de ces données pourraient ne pas être disponibles.
    - colonne 11-12 : `LocalName, GovernmentForm (char(45) NOT NULL DEFAULT '')`: LocalName et GovernmentForm sont des chaînes de caractères de 45 caractères. Elle indique le type de gouvernement en place.
    - colonne 13-14-15 : `HeadOfState (char(60) DEFAULT NULL), Capital (int DEFAULT NULL), Code2 (char(2) NOT NULL DEFAULT '')`: HeadOfState et Capital peuvent être nulles, tandis que Code2 (iso alpha 2) est non nulle. HeadOfState est un champ de 60 caractères, Capital est un entier (pour stocker un identifiant de ville), et Code2 est un champ de 2 caractères (un code de pays alternatif).



#### Table 3 : countrylanguage,
    cette table contient 4 colonnes :
    - colonne 1 : `CountryCode (char(3) NOT NULL DEFAULT '')`:
    CountryCode (char(3) NOT NULL DEFAULT ''): CountryCode est le code iso alpha 3
    - colonne 2 : `Language (char(30) NOT NULL DEFAULT '')`: Language est la langue du pays
    - colonne 3 : `IsOfficial (enum('T','F') NOT NULL DEFAULT 'F')`: IsOfficial est un type énuméré (enum) avec deux valeurs possibles : 'T' (vrai) et 'F' (faux). Cette colonne est destinée à indiquer si la langue est officielle dans le pays concerné.
    - colonne 4 : `Percentage (decimal(4,1) NOT NULL DEFAULT '0.0')`: Percentage est une colonne de type décimal (decimal) utilisée pour représenter le pourcentage de la population du pays parlant la langue. Le format decimal(4,1) signifie qu'il peut stocker jusqu'à quatre chiffres au total, dont un après la virgule.

    Contrainte :
    Dans la table city :
        CountryCode est une clé étrangère qui fait référence à Code dans la table country.
        Cette clé étrangère garantit que toute valeur entrée dans le champ CountryCode de la table city doit déjà exister dans le champ Code de la table country. Donc on ne peut pas avoir une ville dans la table city qui fait référence à un code de pays qui n'existe pas dans la table country.

    Dans la table countrylanguage :
        CountryCode est une clé étrangère qui fait référence également à Code dans la table country.
        Cette relation assure que toute langue spécifiée dans la table countrylanguage est associée à un code de pays valide présent dans la table country. Cela garantit l'intégrité des données en s'assurant que chaque langue est liée à un pays existant.

## Question :

1. Liste des type de gouvernement avec nombre de pays pour chaque.

```SQL
SELECT GovernmentForm, COUNT(*) AS Nombre_de_pays
FROM country
GROUP BY GovernmentForm ORDER BY Nombre_de_pays DESC;
```

| GovernmentForm                               | Nombre_de_pays |
|----------------------------------------------|----------------|
| Republic                                     |            123 |
| Constitutional Monarchy                      |             29 |
| Federal Republic                             |             14 |
| Dependent Territory of the UK                |             12 |
| Monarchy                                     |              5 |
| Overseas Department of France                |              4 |
| Nonmetropolitan Territory of France          |              4 |
| Constitutional Monarchy, Federation          |              4 |
| Territory of Australia                       |              4 |
| Socialistic Republic                         |              3 |
| Nonmetropolitan Territory of New Zealand     |              3 |
| US Territory                                 |              3 |
| Commonwealth of the US                       |              2 |
| Territorial Collectivity of France           |              2 |
| Nonmetropolitan Territory of The Netherlands |              2 |
| Dependent Territory of Norway                |              2 |
| Monarchy (Sultanate)                         |              2 |
| Part of Denmark                              |              2 |
| Special Administrative Region of China       |              2 |
| Islamic Republic                             |              2 |
| Federation                                   |              1 |
| Socialistic State                            |              1 |
| Autonomous Area                              |              1 |
| Administrated by the UN                      |              1 |
| Dependent Territory of the US                |              1 |
| Independent Church State                     |              1 |
| Parlementary Monarchy                        |              1 |
| Constitutional Monarchy (Emirate)            |              1 |
| Occupied by Marocco                          |              1 |
| People'sRepublic                             |              1 |
| Monarchy (Emirate)                           |              1 |
| Co-administrated                             |              1 |
| Emirate Federation                           |              1 |
| Parliamentary Coprincipality                 |              1 |
| Islamic Emirate                              |              1 |

2. Pourquoi countrylanguage.IsOfficial utilise un enum et pas un bool ?

la base de donnée étant créé à une date antérieure à la mise à jour de SQL qui intégre les booléen, c'est la principale raison, pour laquelle l'utilisation d'un enum est privilégié.

3. D’apres la BDD, combien de personne dans le monde parle anglais ?

```SQL
SELECT SUM(c.Population * cl.Percentage / 100) AS Total_personne_parlant_Anglais
FROM country c
JOIN countrylanguage cl ON c.Code = cl.CountryCode
WHERE cl.Language = 'English';
```

Total_personne_parlant_Anglais 347077867.3

4. Faire la liste des langues avec le nombre de locuteur, de la plus parlée à la moins parlée.

| Language                  | Total_Locuteur   |
|---------------------------|------------------|
| Chinese                   | 1191843539.00000 |
| Hindi                     |  405633070.00000 |
| Spanish                   |  355029462.00000 |
| English                   |  347077867.30000 |
| Arabic                    |  233839238.70000 |
| Bengali                   |  209304719.00000 |
| Portuguese                |  177595269.40000 |
| Russian                   |  160807561.30000 |
| Japanese                  |  126814108.00000 |
| Punjabi                   |  104025371.00000 |
| German                    |   92133584.70000 |
| Javanese                  |   83570158.00000 |
| Telugu                    |   79065636.00000 |
| Marathi                   |   75019094.00000 |
| Korean                    |   72291372.00000 |
| Vietnamese                |   70616218.00000 |
| French                    |   69980880.40000 |
| Tamil                     |   68691536.00000 |
| Urdu                      |   63589470.00000 |
| Turkish                   |   62205657.20000 |
| Italian                   |   59864483.20000 |
| Gujarati                  |   48655776.00000 |
| Malay                     |   41517994.00000 |
| Kannada                   |   39532818.00000 |
| Polish                    |   39525035.10000 |
| Ukrainian                 |   36593408.00000 |
| Malajalam                 |   36491832.00000 |
| Thai                      |   33996960.00000 |
| Sunda                     |   33512906.00000 |
| Orija                     |   33450846.00000 |
| Pashto                    |   32404553.00000 |
| Burmese                   |   31471590.00000 |
| Persian                   |   31124734.00000 |
| Hausa                     |   29225396.00000 |
| Joruba                    |   24868874.00000 |
| Ful                       |   23704612.00000 |
| Romanian                  |   23457777.30000 |
| Uzbek                     |   22535760.00000 |
| Pilipino                  |   22258331.00000 |
| Dutch                     |   21388666.00000 |
| Ibo                       |   20182586.00000 |
| Lao                       |   20167307.00000 |
| Oromo                     |   19395150.00000 |
| Kurdish                   |   19062628.00000 |
| Azerbaijani               |   19014911.00000 |
| Amhara                    |   18769500.00000 |
| Sindhi                    |   18464994.00000 |
| Zhuang                    |   17885812.00000 |
| Cebuano                   |   17700311.00000 |
| Serbo-Croatian            |   16762504.70000 |

5. En quelle unité est exprimée la surface des pays ?

En faisant une recherche google pour la superficie de la Suisse on trouve 41 285 km², je compare cette donnée à celle de la base de donnée en utilisant la requete SQL suivante :

```SQL
SELECT SurfaceArea FROM country WHERE Name = 'Switzerland';
```
on obtient le résultat suivant : SurfaceArea  41284.00.

On peut donc en déduire que l'unité utilisée dans cette base de donnée est km².

6. Faire la liste des pays qui ont plus 10 000 000 d’hab. avec leur capitale et le % de la population qui habite dans la capitale.

```SQL
SELECT 
    co.Name AS Nom_pays, 
    ci.Name AS Nom_capitale, 
    co.Population AS Population_pays, 
    ci.Population AS Population_capitale, 
    (ci.Population / co.Population * 100) AS Pourcentage_pop_dans_capitale
FROM country co JOIN city ci ON co.Capital = ci.ID WHERE co.Population > 10000000;
```

Voici la liste des 10 premières lignes de la réponse obtenue avec la requete SQL.

| Nom_pays                              | Nom_capitale        | Population_pays | Population_capitale | Pourcentage_pop_dans_capitale |
|---------------------------------------|---------------------|-----------------|---------------------|-------------------------------|
| Afghanistan                           | Kabul               |        22720000 |             1780000 |                        7.8345 |
| Angola                                | Luanda              |        12878000 |             2022000 |                       15.7012 |
| Argentina                             | Buenos Aires        |        37032000 |             2982146 |                        8.0529 |
| Australia                             | Canberra            |        18886000 |              322723 |                        1.7088 |
| Belgium                               | Bruxelles [Brussel] |        10239000 |              133859 |                        1.3073 |
| Burkina Faso                          | Ouagadougou         |        11937000 |              824000 |                        6.9029 |
| Bangladesh                            | Dhaka               |       129155000 |             3612850 |                        2.7973 |
| Belarus                               | Minsk               |        10236000 |             1674000 |                       16.3540 |
| Brazil                                | Brasília            |       170115000 |             1969868 |                        1.1580 |
| Canada                                | Ottawa              |        31147000 |              335277 |                        1.0764 |

Pour la ligne 5 correspondant à la Belgique, nous avons le nom de la capitale suivit du nom entre [] qui correspond au nom en néerlandais. (Merci google).

7. Liste des 10 pays avec le plus fort taux de croissance entre n et n-1 avec le % de croissance.

```SQL
SELECT Name, GNP, GNPOld, ((GNP - GNPOld) / GNPOld * 100) AS Taux_de_croissance
FROM country WHERE GNPOld > 0 
ORDER BY Taux_de_croissance DESC LIMIT 10;
```

| Name                                  | GNP       | GNPOld    | Taux_de_croissance |
|---------------------------------------|-----------|-----------|--------------------|
| Congo, The Democratic Republic of the |   6964.00 |   2474.00 |         181.487470 |
| Turkmenistan                          |   4397.00 |   2000.00 |         119.850000 |
| Tajikistan                            |   1990.00 |   1056.00 |          88.446970 |
| Estonia                               |   5328.00 |   3371.00 |          58.053990 |
| Albania                               |   3205.00 |   2500.00 |          28.200000 |
| Suriname                              |    870.00 |    706.00 |          23.229462 |
| Iran                                  | 195746.00 | 160151.00 |          22.225899 |
| Bulgaria                              |  12178.00 |  10169.00 |          19.756122 |
| Honduras                              |   5333.00 |   4697.00 |          13.540558 |
| Latvia                                |   6398.00 |   5639.00 |          13.459833 |

8. Liste des pays plurilingues avec pour chacun le nombre de langues parlées.

```SQL
SELECT 
    c.Name AS Nom_pays, 
    COUNT(cl.Language) AS Nombre_de_langues
FROM country c
JOIN countrylanguage cl ON c.Code = cl.CountryCode
GROUP BY c.Name HAVING COUNT(cl.Language) > 1
ORDER BY Nombre_de_langues DESC;
```

Voici les 10 premières lignes du résultat de la requete :

| Nom_pays                              | Nombre_de_langues |
|---------------------------------------|-------------------|
| Canada                                |                12 |
| China                                 |                12 |
| India                                 |                12 |
| Russian Federation                    |                12 |
| United States                         |                12 |
| Tanzania                              |                11 |
| South Africa                          |                11 |
| Congo, The Democratic Republic of the |                10 |
| Iran                                  |                10 |
| Kenya                                 |                10 |

9. Liste des pays avec plusieurs langues officielles, le nombre de langues officielle et le nombre de langues du pays.

```SQL
SELECT 
    c.Name AS Nom_pays, 
    COUNT(cl.Language) AS Langues_totales,
    SUM(CASE WHEN cl.IsOfficial = 'T' THEN 1 ELSE 0 END) AS Langues_officieles
FROM country c
JOIN countrylanguage cl ON c.Code = cl.CountryCode
GROUP BY c.Name HAVING SUM(CASE WHEN cl.IsOfficial = 'T' THEN 1 ELSE 0 END) > 1;
```

| Nom_pays             | Langues_totales | Langues_officieles |
|----------------------|-----------------|--------------------|
| Afghanistan          |               5 |                  2 |
| Netherlands Antilles |               3 |                  2 |
| American Samoa       |               3 |                  2 |
| Burundi              |               3 |                  2 |
| Belgium              |               6 |                  3 |
| Belarus              |               4 |                  2 |
| Bolivia              |               4 |                  3 |
| Canada               |              12 |                  2 |
| Switzerland          |               4 |                  4 |
| Cyprus               |               2 |                  2 |
| Finland              |               5 |                  2 |
| Faroe Islands        |               2 |                  2 |
| Greenland            |               2 |                  2 |
| Guam                 |               5 |                  2 |
| Ireland              |               2 |                  2 |
| Israel               |               3 |                  2 |
| Kyrgyzstan           |               7 |                  2 |
| Sri Lanka            |               3 |                  2 |
| Lesotho              |               3 |                  2 |
| Luxembourg           |               5 |                  3 |
| Madagascar           |               2 |                  2 |
| Marshall Islands     |               2 |                  2 |
| Malta                |               2 |                  2 |
| Nauru                |               5 |                  2 |
| Peru                 |               3 |                  3 |
| Palau                |               4 |                  2 |
| Paraguay             |               4 |                  2 |
| Romania              |               6 |                  2 |
| Rwanda               |               2 |                  2 |
| Singapore            |               3 |                  3 |
| Somalia              |               2 |                  2 |
| Seychelles           |               3 |                  2 |
| Togo                 |               8 |                  2 |
| Tonga                |               2 |                  2 |
| Tuvalu               |               3 |                  2 |
| Vanuatu              |               3 |                  3 |
| Samoa                |               3 |                  2 |
| South Africa         |              11 |                  4 |

10. Liste des langues parlées en France avec le % pour chacune.

```SQL 
SELECT Language, Percentage
FROM countrylanguage
WHERE CountryCode = (SELECT Code FROM country WHERE Name = 'France');
```

| Language   | Percentage |
|------------|------------|
| Arabic     |        2.5 |
| French     |       93.6 |
| Italian    |        0.4 |
| Portuguese |        1.2 |
| Spanish    |        0.4 |
| Turkish    |        0.4 |

11. Liste des langues parlées en Chine avec le % pour chacune.

```SQL 
SELECT Language, Percentage
FROM countrylanguage
WHERE CountryCode = (SELECT Code FROM country WHERE Name = 'China');
```

| Language  | Percentage |
|-----------|------------|
| Chinese   |       92.0 |
| Dong      |        0.2 |
| Hui       |        0.8 |
| Mantšu    |        0.9 |
| Miao      |        0.7 |
| Mongolian |        0.4 |
| Puyi      |        0.2 |
| Tibetan   |        0.4 |
| Tujia     |        0.5 |
| Uighur    |        0.6 |
| Yi        |        0.6 |
| Zhuang    |        1.4 |

12. Liste des langues parlées en Etats-Unis avec le % pour chacune.

```SQL 
SELECT Language, Percentage
FROM countrylanguage
WHERE CountryCode = (SELECT Code FROM country WHERE Name = 'United States');
```

| Language   | Percentage |
|------------|------------|
| Chinese    |        0.6 |
| English    |       86.2 |
| French     |        0.7 |
| German     |        0.7 |
| Italian    |        0.6 |
| Japanese   |        0.2 |
| Korean     |        0.3 |
| Polish     |        0.3 |
| Portuguese |        0.2 |
| Spanish    |        7.5 |
| Tagalog    |        0.4 |
| Vietnamese |        0.2 |

13. Liste des langues parlées en Royaume-Uni avec le % pour chacune.

```SQL 
SELECT Language, Percentage
FROM countrylanguage
WHERE CountryCode = (SELECT Code FROM country WHERE Name = 'United Kingdom');
```

| Language | Percentage |
|----------|------------|
| English  |       97.3 |
| Gaeli    |        0.1 |
| Kymri    |        0.9 |

14. Pour chaque région quelle est la langue la plus parler et quel pourcentage de la population la parle.

```SQL
SELECT 
    sub.Region,
    sub.Language,
    sub.Pourcentage
FROM (
    SELECT 
        lr.Region,
        lr.Language,
        (lr.TotalPopulationSpeakingLanguage * 100) / pr.TotalPopulation AS Pourcentage,
        ROW_NUMBER() OVER (PARTITION BY lr.Region ORDER BY (lr.TotalPopulationSpeakingLanguage * 100) / pr.TotalPopulation DESC) AS rn
    FROM (
        SELECT 
            c.Region,
            cl.Language,
            SUM((cl.Percentage / 100) * c.Population) AS TotalPopulationSpeakingLanguage
        FROM 
            countrylanguage cl
        JOIN 
            country c ON cl.CountryCode = c.Code
        GROUP BY 
            c.Region, cl.Language
    ) AS lr
    JOIN (
        SELECT 
            c.Region,
            SUM(c.Population) AS TotalPopulation
        FROM 
            country c
        GROUP BY 
            c.Region
    ) AS pr ON lr.Region = pr.Region
) AS sub
WHERE 
    sub.rn = 1
ORDER BY 
    sub.Region;
```

15. Est-ce que la somme des pourcentages de langues parlées dans un pays est égale à 100 ? Pourquoi ?

```SQL
SELECT 
    c.Name AS CountryName, 
    SUM(cl.Percentage) AS Total_pourcentage_langue
FROM country c
JOIN countrylanguage cl ON c.Code = cl.CountryCode
WHERE c.Name = 'France'
GROUP BY c.Name;
```

Avec cette requete nous pouvons constater que l'addition de tout les pourcentages est de 98.5, cela implique que certaines langues parlées en France ne sont juste pas indiquées.

16. Faire une carte du monde, avec une couleur par région. (chaque pays étant dans la bonne couleur).

[la carte](./map.html)