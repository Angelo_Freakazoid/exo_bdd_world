import pandas as pd
import plotly.express as px
import mysql.connector

conn = mysql.connector.connect(
    host='localhost',
    user='alucard',
    password='coucou123',
    database='world'
)

query = "SELECT Name, Code, Region FROM country"
df = pd.read_sql(query, conn)

conn.close()

fig = px.choropleth(df, locations="Code",
                    color="Region", 
                    hover_name="Region",
                    color_continuous_scale=px.colors.sequential.Plasma)
fig.write_html("./map.html")