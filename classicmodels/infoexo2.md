# Info sur exo modification et migration de bdd

## classicmodel v1

init.sql

![Voici le diagrame de la BDD v1.](classicmodels/classicmodelsv1.png "Diagrame")

## classicmodel v2

après application de migration.sql

![Voici le diagrame de la BDD v2.](classicmodels/classicmodelsv2.png "Diagrame")

## ordre d'execution des fichiers pour avoir la bdd classicmodelsv2 :

penser à lancer le fichier acces.sql pour donner les droits à toutes les bdd.
penser à lancer le fichier install.sh à la racine du projet.

## questions 1-8 :

    1 Pour chaque pays (avec au moins 1 client) le nombre de client, la langue officielle du pays (si plusieurs, peu importe laquelle), le CA total.

```sql
    SELECT
        country,
        count(DISTINCT c.customerNumber) AS `nombre de client`,
        group_concat(DISTINCT language) AS `langue`,
        sum(od.priceEach*quantityOrdered) AS `CA`
    FROM customers AS c
    JOIN orders AS o ON c.customerNumber = o.customerNumber
    JOIN orderdetails AS od ON od.orderNumber = o.orderNumber 
    GROUP BY country;
```

| country | nombre de client | langue | CA         |
|:--------|-----------------:|:------:|-----------:|
| AUS     |                5 | en     |  562582.59 |
| AUT     |                2 | de     |  188540.06 |
| BEL     |                2 | nl     |  100068.76 |
| CAN     |                3 | en     |  205911.86 |
| CHE     |                1 | de     |  108777.92 |
| DEU     |                3 | de     |  196470.99 |
| DNK     |                2 | da     |  218994.92 |
| ESP     |                5 | es     | 1099389.09 |
| FIN     |                3 | fi     |  295149.35 |
| FRA     |               12 | fr     | 1007374.02 |
| GBR     |                5 | en     |  436947.44 |
| HKG     |                1 | en     |   45480.79 |
| IRL     |                1 | en     |   49898.27 |
| ITA     |                4 | it     |  360616.81 |
| JPN     |                2 | ja     |  167909.95 |
| NOR     |                3 | no     |  270846.30 |
| NZL     |                4 | en     |  476847.01 |
| PHL     |                1 | tl     |   87468.30 |
| SGP     |                2 | zh     |  263997.78 |
| SWE     |                2 | sv     |  187638.35 |
| USA     |               35 | en     | 3273280.05 |

    2 La liste des 10 clients qui ont rapporté le plus gros CA, avec le CA correspondant.

```sql
    SELECT 
        c.customerName, 
        sum(od.priceEach*quantityOrdered) AS `CA`
    FROM customers AS c
    JOIN orders AS o ON c.customerNumber = o.customerNumber
    JOIN orderdetails AS od ON od.orderNumber = o.orderNumber
    GROUP BY c.customerName
    ORDER BY `CA` DESC 
    LIMIT 10;
```

| customerName                 | CA        |
|------------------------------|-----------|
| Euro+ Shopping Channel       | 820689.54 |
| Mini Gifts Distributors Ltd. | 591827.34 |
| Australian Collectors, Co.   | 180585.07 |
| Muscle Machine Inc           | 177913.95 |
| La Rochelle Gifts            | 158573.12 |
| Dragon Souveniers, Ltd.      | 156251.03 |
| Down Under Souveniers, Inc   | 154622.08 |
| Land of Toys Inc.            | 149085.15 |
| AV Stores, Co.               | 148410.09 |
| The Sharp Gifts Warehouse    | 143536.27 |

    3 La durée moyenne (en jours) entre les dates de commandes et les dates d’expédition (de la même commande).

```sql
    SELECT avg(datediff(shippedDate,orderDate)) AS `durée moyenne en jours entre les dates de commande et d'expédition`
    FROM orders;
```

| durée moyenne en jours entre les dates de commande et d'expédition   |
|----------------------------------------------------------------------|
|                                                               3.7564 |

    4 Les 10 produits les plus vendus.

```sql
    SELECT p.productName, sum(pl.quantityOrdered) AS qt
    FROM orderdetails AS pl
    JOIN products AS p ON p.productCode = pl.productCode
    GROUP BY pl.productCode
    ORDER BY qt DESC
    LIMIT 10;
```

| productName                             | qt   |
|-----------------------------------------|------|
| 1992 Ferrari 360 Spider red             | 1808 |
| 1937 Lincoln Berline                    | 1111 |
| American Airlines: MD-11S               | 1085 |
| 1941 Chevrolet Special Deluxe Cabriolet | 1076 |
| 1930 Buick Marquette Phaeton            | 1074 |
| 1940s Ford truck                        | 1061 |
| 1969 Harley Davidson Ultimate Chopper   | 1057 |
| 1957 Chevy Pickup                       | 1056 |
| 1964 Mercedes Tour Bus                  | 1053 |
| 1956 Porsche 356A Coupe                 | 1052 |

    5 Pour chaque pays le produits le plus vendu.

```sql
    SELECT s.country, s.productName, s.qt
    FROM (
        SELECT 
            c.country, 
            p.productName, 
            SUM(pl.quantityOrdered) AS qt,
            dense_rank() OVER (PARTITION BY c.country ORDER BY SUM(pl.quantityOrdered) DESC) AS rn
        FROM orderdetails AS pl
        JOIN orders AS o ON o.orderNumber = pl.orderNumber
        JOIN customers AS c ON c.customerNumber = o.customerNumber
        JOIN products AS p ON p.productCode = pl.productCode
        GROUP BY 
            c.country, 
            p.productCode
    ) AS s WHERE s.rn = 1;
```

| country | productName                                 | qt   |
|---------|---------------------------------------------|------|
| AUS     | 1913 Ford Model T Speedster                 |  231 |
| AUT     | 1969 Dodge Charger                          |  128 |
| BEL     | 1941 Chevrolet Special Deluxe Cabriolet     |   95 |
| CAN     | 1996 Peterbilt 379 Stake Bed with Outrigger |   97 |
| CAN     | 1982 Camaro Z28                             |   97 |
| CHE     | 1948 Porsche 356-A Roadster                 |   91 |
| DEU     | 1971 Alpine Renault 1600s                   |  117 |
| DNK     | 1948 Porsche Type 356 Roadster              |  101 |
| ESP     | 1992 Ferrari 360 Spider red                 |  336 |
| FIN     | 1992 Ferrari 360 Spider red                 |  141 |
| FRA     | 2002 Yamaha YZR M1                          |  278 |
| GBR     | 1952 Citroen-15CV                           |  162 |
| HKG     | 1928 British Royal Navy Airplane            |   79 |
| IRL     | 1970 Triumph Spitfire                       |   50 |
| ITA     | 1928 British Royal Navy Airplane            |  162 |
| JPN     | American Airlines: MD-11S                   |   92 |
| NOR     | 1969 Harley Davidson Ultimate Chopper       |   89 |
| NZL     | 1969 Dodge Super Bee                        |  142 |
| PHL     | 1957 Corvette Convertible                   |   94 |
| SGP     | 1940s Ford truck                            |  128 |
| SWE     | 1940s Ford truck                            |   97 |
| USA     | 1957 Chevy Pickup                           |  523 |

    6 Le produit qui a rapporté le plus de bénéfice.

```sql
    SELECT p.productName, SUM((od.priceEach - p.buyPrice)*od.quantityOrdered) AS benef
    FROM orderdetails AS od
    JOIN products AS p ON p.productCode = od.productCode
    GROUP BY p.productCode
    ORDER BY benef DESC LIMIT 1;
```

| productName                 | benef     |
|-----------------------------|-----------|
| 1992 Ferrari 360 Spider red | 135996.78 |

    7 La moyenne des différences entre le MSRP et le prix de vente (en pourcentage).

```sql
    SELECT p.productName, avg(p.MSRP - od.priceEach) AS avgdif
    FROM products AS p
    JOIN orderdetails AS od ON p.productCode = od.productCode
    GROUP BY p.productCode ORDER BY avgdif DESC;
```

     voici un échantillon de 10 lignes des 109 lignes de la réponse.

| productName                                 | avgdif    |
|---------------------------------------------|-----------|
| 1968 Ford Mustang                           | 22.123704 |
| 2003 Harley-Davidson Eagle Drag Bike        | 21.371786 |
| 2001 Ferrari Enzo                           | 20.703333 |
| 1993 Mazda RX-7                             | 20.234444 |
| 1998 Chrysler Plymouth Prowler              | 19.587143 |
| 1957 Corvette Convertible                   | 19.509259 |
| 1928 Mercedes-Benz SSK                      | 18.862500 |
| 1980s Black Hawk Helicopter                 | 18.584643 |
| 2002 Suzuki XREO                            | 18.451071 |
| 1992 Ferrari 360 Spider red                 | 16.997170 |



    8 Pour chaque pays (avec au moins 1 client) le nombre de bureaux dans le même pays.

```sql
    SELECT o.country, count(*) AS `nombre de bureaux`
    FROM offices AS o
    WHERE (
        SELECT count(*)
        FROM customers AS cc
        WHERE cc.country = o.country
    ) > 1
    GROUP BY o.country;
```

| country | nombre de bureaux |
|---------|-------------------|
| USA     |                 3 |
| FRA     |                 1 |
| JPN     |                 1 |
| AUS     |                 1 |
| GBR     |                 1 |