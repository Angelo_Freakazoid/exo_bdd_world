import mysql.connector
import pandas as pd

world_db_config = {
    'host': 'localhost',
    'user': 'alucard',
    'password': 'coucou123',
    'database': 'world'
}

classicmodels_db_config = {
    'host': 'localhost',
    'user': 'alucard',
    'password': 'coucou123',
    'database': 'classicmodelsv2'
}

def get_official_languages():
    query = """
select t.code as CountryCode, t.language as language 
from(
    select c.code, 
        cl.language, 
        cl.percentage,
        ROW_NUMBER() OVER (PARTITION BY c.code ORDER BY cl.percentage DESC) AS rn
    from world.country as c 
    join world.countrylanguage as cl on cl.countrycode = c.code 
    where cl.isOfficial = 'T'
) as t
where t.rn = 1;
"""
    with mysql.connector.connect(**world_db_config) as conn:
        return pd.read_sql(query, conn)

def update_customer_language():
    official_languages = get_official_languages()

    language_codes_df = pd.read_csv('classicmodels/language-codes_csv.csv')
    language_codes_df["English"] = language_codes_df["English"].str.split(";")
    language_codes_df = language_codes_df.explode("English")    
    languages_with_codes = official_languages.merge(language_codes_df, left_on='language', right_on='English')
    country_language_map = dict(zip(languages_with_codes['CountryCode'], languages_with_codes['alpha2']))
    with mysql.connector.connect(**classicmodels_db_config) as conn:
        cursor = conn.cursor()
        for country_code, language_code in country_language_map.items():
            update_query = """
            UPDATE customers
            SET language = %s
            WHERE country = %s
            """
            cursor.execute(update_query, (language_code, country_code))
        conn.commit()

if __name__ == "__main__":
    df = update_customer_language()
