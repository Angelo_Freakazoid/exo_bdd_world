INSERT INTO classicmodelsv2.productlines TABLE classicmodels.productlines;

INSERT INTO classicmodelsv2.products TABLE classicmodels.products;

UPDATE classicmodels.offices SET country = "United Kingdom" WHERE country = "UK";
UPDATE classicmodels.offices SET country = "United States" WHERE country = "USA";

INSERT INTO classicmodelsv2.offices 
SELECT
    officeCode,
    city,
    phone,
    addressLine1,
    addressLine2,
    state,
    world.country.Code,
    postalCode,
    territory
FROM    
    classicmodels.offices
JOIN 
    world.country ON world.country.Name = classicmodels.offices.country;

INSERT INTO classicmodelsv2.employees TABLE classicmodels.employees;

UPDATE classicmodels.customers SET country = "United Kingdom" WHERE country = "UK";
UPDATE classicmodels.customers SET country = "United States" WHERE country = "USA";
UPDATE classicmodels.customers SET CountrY = "Russian Federation" WHERE country = "Russia";
UPDATE classicmodels.customers SET CountrY = "Norway" WHERE country = "Norway  ";


INSERT INTO classicmodelsv2.customers
SELECT
    c.customerNumber,
    c.customerName,
    c.contactLastName,
    c.contactFirstName,
    c.phone,
    c.addressLine1,
    c.addressLine2,
    c.city,
    c.state,
    c.postalCode,
    w.Code,
    c.salesRepEmployeeNumber,
    c.creditLimit,
    NULL
FROM    
    classicmodels.customers AS c
JOIN 
    world.country AS w ON w.Name = c.country;

INSERT INTO classicmodelsv2.payments TABLE classicmodels.payments;

INSERT INTO classicmodelsv2.orders TABLE classicmodels.orders; 

INSERT INTO classicmodelsv2.orderdetails TABLE classicmodels.orderdetails; 

