USE classicmodelsv2;

ALTER TABLE customers MODIFY country CHAR(3) NOT NULL;

ALTER TABLE offices MODIFY country CHAR(3) NOT NULL;

-- SELECT DISTINCT status FROM orders; requete sql permettant de récupérer l'énum demandée.
ALTER TABLE orders MODIFY COLUMN status ENUM('Shipped', 'Resolved', 'Cancelled', 'On Hold', 'Disputed', 'In Process') DEFAULT 'On Hold' NOT NULL;

ALTER TABLE customers ADD language CHAR(2) DEFAULT NULL;