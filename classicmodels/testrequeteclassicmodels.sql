-- 1. Pour chaque pays (avec au moins 1 client) le nombre de client, la langue officielle du pays (si plusieurs, peu importe laquelle), le CA total.

SELECT
    country,
    count(DISTINCT c.customerNumber) AS `nombre de client`,
    group_concat(DISTINCT language) AS `langue`,
    sum(od.priceEach*quantityOrdered) AS `CA`
FROM customers AS c
JOIN orders AS o ON c.customerNumber = o.customerNumber
JOIN orderdetails AS od ON od.orderNumber = o.orderNumber 
GROUP BY country;

-- 2. La liste des 10 clients qui ont rapporté le plus gros CA, avec le CA correspondant.

SELECT 
    c.customerName, 
    sum(od.priceEach*quantityOrdered) AS `CA`
FROM customers AS c
JOIN orders AS o ON c.customerNumber = o.customerNumber
JOIN orderdetails AS od ON od.orderNumber = o.orderNumber
GROUP BY c.customerName
ORDER BY `CA` DESC 
LIMIT 10;

-- 3. La durée moyenne (en jours) entre les dates de commandes et les dates d’expédition (de la même commande).

SELECT avg(datediff(shippedDate,orderDate)) AS `durée moyenne en jours entre les dates de commande et d'expédition`
FROM orders;

-- 4. Les 10 produits les plus vendus.

SELECT p.productName, sum(pl.quantityOrdered) AS qt
FROM orderdetails AS pl
JOIN products AS p ON p.productCode = pl.productCode
GROUP BY pl.productCode
ORDER BY qt DESC
LIMIT 10;

-- 5. Pour chaque pays le produits le plus vendu.

SELECT s.country, s.productName, s.qt
FROM (
    SELECT 
        c.country, 
        p.productName, 
        SUM(pl.quantityOrdered) AS qt,
        dense_rank() OVER (PARTITION BY c.country ORDER BY SUM(pl.quantityOrdered) DESC) AS rn
    FROM orderdetails AS pl
    JOIN orders AS o ON o.orderNumber = pl.orderNumber
    JOIN customers AS c ON c.customerNumber = o.customerNumber
    JOIN products AS p ON p.productCode = pl.productCode
    GROUP BY 
        c.country, 
        p.productCode
) AS s WHERE s.rn = 1;

-- 6. Le produit qui a rapporté le plus de bénéfice.

SELECT p.productName, SUM((od.priceEach - p.buyPrice)*od.quantityOrdered) AS benef
FROM orderdetails AS od
JOIN products AS p ON p.productCode = od.productCode
GROUP BY p.productCode
ORDER BY benef DESC LIMIT 1;

-- 7. La moyenne des différences entre le MSRP et le prix de vente (en pourcentage).

SELECT p.productName, avg(p.MSRP - od.priceEach) AS avgdif
FROM products AS p
JOIN orderdetails AS od ON p.productCode = od.productCode
GROUP BY p.productCode ORDER BY avgdif DESC;

-- 8. Pour chaque pays (avec au moins 1 client) le nombre de bureaux dans le même pays.-- 

SELECT o.country, count(*) AS `nombre de bureaux`
FROM offices AS o
WHERE (
    SELECT count(*)
    FROM customers AS cc
    WHERE cc.country = o.country
) > 1
GROUP BY o.country;